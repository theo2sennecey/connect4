#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

#include <assert.h>


void print_plateau(int lines, int cols, int *plateau) {
    for (int i = 0; i < lines; i++) {
        for (int j = (i * cols); j < (cols * i) + cols; j++) {
            printf(" %d ", plateau[j]);
        }
        printf("\n");
    }
   
}

int get_pawn_position(int col, int lines, int cols, int *plateau) {
    int start = (lines * cols) - cols + col - 1;
    for (int i = 0; i < lines; i++) {      
        if (plateau[start - (i * cols)] == 0) {
            return start - (i * cols);
        }
    }
    return -1;
}

int main(int ac, char **av) {
    // av[1]: lines, av[2]: cols;
    if (ac != 3) {
        printf("error: wrong arguments!\n");
        printf("usage: ./connect4 <lines> <cols>\n");
        return 1;
    }
    int lines = atoi(av[1]);
    int cols = atoi(av[2]);
    if ((lines > 44) || (lines < 6))
        return 1;
    if ((cols > 158) || (cols < 7))
        return 1;
    // fill plateau:
    int plateau[lines * cols];
    memset((int *)plateau, 0, (lines * cols) * 4);
    // game loop
    bool state = true;
    char buf[5];
    int ret = 0;
    print_plateau(lines, cols, (int *)plateau);
    while (state) {
        // 1: read user input: col number
        printf("Please select a column: \n");
        ret = read(1, &buf, 4);
        if (ret) {
            for (int i = 0; i < 4; i++) {
                if (!isdigit(buf[i])) {
                    ret = -1;
                    break;
                }
            }
            if (ret) {
                if ((atoi(buf) <= 158) && (atoi(buf) <= cols)) {
                    int pos = get_pawn_position(atoi(buf), lines, cols, (int *)plateau);
                    if (pos == -1)
                        printf("could not place pawn, this column is full!\n");
                    else {
                        plateau[pos] = 1;
                    }
                }
            }
        }    
        // 2: update the plateau
        print_plateau(lines, cols, (int *)plateau);
        /* print plateau, and check if won, or if the plateau is full (then it's a draw) */
        // 3: calculate best score for the next move
        /* pick a strategy (defend, or attack) => pick a move */
        // 4: update the plateau
        /* print plateau, and check if won, or if the plateau is full (then it's a draw) */
        bzero(buf, 4);
    }
}
